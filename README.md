# LoRaWAN-Sample-Project

This repository contains a working example LoRaWAN application for a `TTGO LoRa32-OLED v2.1.6` end device.

It is based on [LMIC-node](https://github.com/lnlp/LMIC-node).
Many parts were removed for easier understandability.
The original projects documentation is rather comprehensive and definitely a recommended resource.


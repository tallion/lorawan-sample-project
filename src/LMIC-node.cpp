/*******************************************************************************
 *  File:          LMIC-node.cpp
 *
 *  License:       MIT License.
 * 
 *  Orig. author:  Leonel Lopes Parente
 * 
 *  Description:   Example for MCCI Library with PlatformIO
 ******************************************************************************/

#include "LMIC-node.h"
#include "printing.h"


const uint8_t payloadBufferLength = 32;    // Adjust to fit max payload length

uint8_t payloadBuffer[payloadBufferLength];

// Note: LoRa module pin mappings are defined in the Board Support Files.

// Set LoRaWAN keys defined in lorawan-keys.h.
static const u1_t PROGMEM DEVEUI[8]  = { OTAA_DEVEUI };
static const u1_t PROGMEM APPEUI[8]  = { OTAA_APPEUI };
static const u1_t PROGMEM APPKEY[16] = { OTAA_APPKEY };

// Below callbacks are used by LMIC for reading above values.
void os_getDevEui(u1_t *buf) { memcpy_P(buf, DEVEUI, 8); }
void os_getArtEui(u1_t *buf) { memcpy_P(buf, APPEUI, 8); }
void os_getDevKey(u1_t *buf) { memcpy_P(buf, APPKEY, 16); }

#include "internal.h" // do not worry about that


//  █ █ █▀▀ █▀▀ █▀▄   █▀▀ █▀█ █▀▄ █▀▀   █▀▄ █▀▀ █▀▀ ▀█▀ █▀█
//  █ █ ▀▀█ █▀▀ █▀▄   █   █ █ █ █ █▀▀   █▀▄ █▀▀ █ █  █  █ █
//  ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀   ▀▀▀ ▀▀▀ ▀▀  ▀▀▀   ▀▀  ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀


void processWork(ostime_t doWorkJobTimeStamp) {
	// This function is called from the doWorkCallback() callback function

	// This is where the main work is performed and uplink messages
	// can be scheduled if anything needs to be transmitted.
	// scheduleUplink(uint8_t fPort, uint8_t *data, uint8_t dataLength) might be a useful function ...

	// Skip processWork if using OTAA and still joining.
	if (LMIC.devaddr != 0) {

		ostime_t timestamp = os_getTime();
		printProcessWork(timestamp);

		// For simplicity LMIC-node will try to send an uplink
		// message every time processWork() is executed.

		if (LMIC.opmode & (OP_POLL | OP_TXDATA | OP_JOINING | OP_TXRXPEND)) {
			// TxRx is currently pending, do not send.
			printEvent(timestamp, "Uplink not scheduled because TxRx pending", PrintTarget::Serial);
			printEvent(timestamp, "UL not scheduled", PrintTarget::Display);
		} else {
			uint8_t fPort = GROUP_NUMBER;

			// ...
		}
	}
}


void processDownlink(ostime_t txCompleteTimestamp, uint8_t fPort, uint8_t *data, uint8_t dataLength) {
	// This function is called from the onEvent() event handler on EV_TXCOMPLETE when a downlink message is received.

	serial.print("Got packet on port:");
	serial.println(fPort);

	// ...

}


//  █ █ █▀▀ █▀▀ █▀▄   █▀▀ █▀█ █▀▄ █▀▀   █▀▀ █▀█ █▀▄
//  █ █ ▀▀█ █▀▀ █▀▄   █   █ █ █ █ █▀▀   █▀▀ █ █ █ █
//  ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀   ▀▀▀ ▀▀▀ ▀▀  ▀▀▀   ▀▀▀ ▀ ▀ ▀▀ 


void setup() {
	bool hardwareInitSucceeded = boardInit(InitType::Hardware);
	initDisplay();
	initSerial(MONITOR_SPEED, WAITFOR_SERIAL_S);
	boardInit(InitType::PostInitSerial);
	printHeader();

	if (!hardwareInitSucceeded) {
		serial.println(F("Error: hardware init failed."));
		serial.flush();
		display.setCursor(COL_0, FRMCNTRS_ROW);
		display.print(F("HW init failed"));
		abort();
	}

	initLmic();
	LMIC_startJoining();
	os_setCallback(&doWorkJob, doWorkCallback);
}


void loop() {
	os_runloop_once();
}


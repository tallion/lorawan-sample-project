#pragma once

#include "LMIC-node.h"
#include "printing.h"


static osjob_t doWorkJob;


void initLmic(bit_t adrEnabled = 1,
              dr_t abpDataRate = DefaultABPDataRate,
              s1_t abpTxPower = DefaultABPTxPower) {
	// ostime_t timestamp = os_getTime();

	// Initialize LMIC runtime environment
	os_init();
	// Reset MAC state
	LMIC_reset();

	// Enable or disable ADR (data rate adaptation).
	// Should be turned off if the device is not stationary (mobile).
	// 1 is on, 0 is off.
	LMIC_setAdrMode(adrEnabled);

	// Register a custom eventhandler and don't use default onEvent() to enable
	// additional features (e.g. make EV_RXSTART available). User data pointer is omitted.
	LMIC_registerEventCb(&onLmicEvent, nullptr);
}


void onLmicEvent(void *pUserData, ev_t ev) {
	// LMIC event handler
	ostime_t timestamp = os_getTime();

	switch (ev) {
		// Only supported in MCCI LMIC library:
		case EV_RXSTART:
			// Do not print anything for this event or it will mess up timing.
			break;
		case EV_TXSTART:
			setTxIndicatorsOn();
			printEvent(timestamp, ev);
			break;
		case EV_JOIN_TXCOMPLETE:
		case EV_TXCANCELED:
			setTxIndicatorsOn(false);
			printEvent(timestamp, ev);
			break;
		case EV_JOINED:
			setTxIndicatorsOn(false);
			printEvent(timestamp, ev);
			printSessionKeys();
			LMIC_setLinkCheckMode(0);
			os_clearCallback(&doWorkJob);
			os_setCallback(&doWorkJob, doWorkCallback);
			break;
		case EV_TXCOMPLETE:
			setTxIndicatorsOn(false);
			printEvent(timestamp, ev);
			printFrameCounters();
			if (LMIC.dataLen != 0 || LMIC.dataBeg != 0) {
				uint8_t fPort = 0;
				if (LMIC.txrxFlags & TXRX_PORT) {
					fPort = LMIC.frame[LMIC.dataBeg - 1];
				}
				printDownlinkInfo();
				processDownlink(timestamp, fPort, LMIC.frame + LMIC.dataBeg, LMIC.dataLen);
			}
			break;
		// Below events are printed only.
		case EV_SCAN_TIMEOUT:
		case EV_JOINING:
		case EV_JOIN_FAILED:
		case EV_REJOIN_FAILED:
		case EV_LOST_TSYNC:
		case EV_RESET:
		case EV_RXCOMPLETE:
			printEvent(timestamp, ev);
			break;
		default:
			printEvent(timestamp, "Misc/Unknown Event");
			break;
	}
}


static void doWorkCallback(osjob_t *job) {
	// Event hander for doWorkJob. Gets called by the LMIC scheduler.
	// The actual work is performed in function processWork() which is called below.

	ostime_t timestamp = os_getTime();
	//serial.println();
	printEvent(timestamp, "doWork job started", PrintTarget::Serial);

	// Do the work that needs to be performed.
	processWork(timestamp);

	// This job must explicitly reschedule itself for the next run.
	ostime_t startAt = timestamp + sec2osticks((int64_t) DO_WORK_INTERVAL_SECONDS);
	os_setTimedCallback(&doWorkJob, startAt, doWorkCallback);
}


lmic_tx_error_t scheduleUplink(uint8_t fPort, uint8_t *data, uint8_t dataLength, bool confirmed = false) {
	// This function is called from the processWork() function to schedule
	// transmission of an uplink message that was prepared by processWork().
	// Transmission will be performed at the next possible time

	ostime_t timestamp = os_getTime();
	printEvent(timestamp, "Packet queued");

	lmic_tx_error_t retval = LMIC_setTxData2(fPort, data, dataLength, confirmed ? 1 : 0);
	timestamp = os_getTime();

	if (retval != LMIC_ERROR_SUCCESS) {
		String errmsg;

		errmsg = "LMIC Error: ";
		errmsg.concat(lmicErrorNames[abs(retval)]);
		printEvent(timestamp, errmsg.c_str(), PrintTarget::Serial);

		errmsg = "LMIC Err: ";
		errmsg.concat(retval);
		printEvent(timestamp, errmsg.c_str(), PrintTarget::Display);
	}
	return retval;
}
